function limitFunctionCallCount(cb,n){
    let count =0 
    return function(){
        count +=1;
        if (count >n){
            return null;
        }else{
            return cb()
        }
    }
}

module.exports = limitFunctionCallCount;

