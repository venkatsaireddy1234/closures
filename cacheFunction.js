function cacheFunction(cb){
    let cache = {};
    return function (arg){
        if (arg in cache){
            return cache[arg];
        }else{
            cache[arg]=cb(arg)
            return cache[arg]
        }
    }
}

module.exports = cacheFunction;